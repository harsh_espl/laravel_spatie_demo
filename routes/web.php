<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home/create', function () {
    return "Add Post";
})->middleware('permission:write post');

Route::get('/home/edit', function () {
    return "Edit Post";
})->middleware('permission:edit post');

Route::get('home/publish', function () {
    return "Publish Post";
})->middleware('permission:publish post');

// Route::group(['middleware' => ['role:admin']], function () {
Route::middleware(['admin'])->group(function () {
    Route::get('home/permission', [PostController::class, 'index']);
    Route::get('home/permission/create', [PostController::class, 'create']);
    Route::post('home/permission', [PostController::class, 'store']);
    Route::get('home/permission/{id}/delete', [PostController::class, 'destroy']);
    Route::get('home/permission/{id}/edit', [PostController::class, 'edit']);
    Route::post('home/permission/update', [PostController::class, 'update']);
});
