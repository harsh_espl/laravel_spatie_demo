@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        {{ __('Dashboard') }}
                        @can('write post')
                            <div><a href="home/create" class="btn btn-secondary btn-sm">Create Post</a></div>
                        @endcan
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <tr class="text-center">
                                <th>Index</th>
                                <th>Body</th>
                                @if (auth()->user()->can('edit post') || auth()->user()->can('publish post'))
                                    <th colspan="2">Action</th>
                                @endif
                            </tr>
                            @forelse (App\Models\Post::all() as $key=>$item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->title }}</td>
                                    @can('edit post')
                                        <td><a href="home/edit" class="btn btn-outline-primary btn-sm">Edit</a></td>
                                    @endcan
                                    @can('publish post')
                                        <td><a href="home/publish" class="btn btn-outline-success btn-sm">Publish</a></td>
                                    @endcan
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">
                                        <h2>No Data</h2>
                                    </td>
                                </tr>
                            @endforelse
                        </table>
                        {{-- {{ __('You are logged in!') }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection