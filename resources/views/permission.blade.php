@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        {{ __('Permission') }}
                        @can('write post')
                            <div><a href="/home/permission/create" class="btn btn-secondary btn-sm">Create Permission</a></div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="text-center mb-2">
                            <a href="/home" class="btn btn-outline-danger">Home</a>
                        </div>
                        <table class="table table-bordered">
                            <tr class="text-center">
                                <th>Index</th>
                                <th>Permission</th>
                                <th>Role</th>
                                <th colspan="2">Action</th>
                            </tr>
                            @forelse($permission as $key=>$item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        @foreach ($items as $data)
                                            @if ($item->name == $data->permission)
                                                {{ ucfirst($data->role) . ' | ' }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td><a href="/home/permission/{{ $item->id }}/edit"
                                            class="btn btn-outline-primary btn-sm">Edit</a></td>
                                    <td><a href="/home/permission/{{ $item->id }}/delete"
                                            class="btn btn-outline-danger btn-sm">Delete</a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">
                                        <h2>No Permission</h2>
                                    </td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
