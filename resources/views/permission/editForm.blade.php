@extends('newhtml')

@section('title', 'Edit Permission')

@section('contents')

<form action="/home/permission/update" method="POST">
    @csrf
    <input type="hidden" name="id" value="{{ $data->id }}">
    @include('permission.form')
    <div>
        <input type="submit" value="Update" class="btn btn-primary">
        <a href="/home/permission" class="btn btn-outline-secondary"> Cancle </a>
    </div>    
</form>
@endsection
