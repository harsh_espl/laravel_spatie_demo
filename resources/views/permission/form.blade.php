<div class="form-group mb-3">
    <label for="permission_name">New Permission Name</label>
    <input type="text" class="form-control @error('permission_name') is-invalid @enderror" name="permission_name"
        value="{{ old('permission_name') ?? $data->name }}" placeholder="Enter Permission">
    <span class="text-danger">@error('permission_name') {{ $message }} @enderror</span>
</div>
<div class="mb-3">
    <label for="">Select Role</label>
    <select class="form-control @error('select_role') is-invalid @enderror" name="select_role[]"
        style="overflow: hidden" size="{{ sizeof($role) - 1 }}" multiple>
        @forelse ($role as $item)
            @if ($item->name == 'admin')
                <option value="{{ $item->id }}" selected hidden>{{ ucfirst($item->name) }}</option>
            @else
                <option value="{{ $item->id }}" {{ isset($rhp) ? (in_array($item->id, $rhp) ? 'selected' : null) : null }}>
                    {{ ucfirst($item->name) }}</option>
            @endif
        @empty
            <option value="" disabled> Data Not Available </option>
        @endforelse
    </select>
    <span class="text-danger">@error('select_role'){{ $message }}@enderror</span>
</div>
