@extends('newhtml')

@section('title', 'Create New Permission')

@section('contents')
<form action="/home/permission" method="POST">
    @csrf
    @include('permission.form')
    <div>
        <input type="submit" value="Create" class="btn btn-primary">
        <a href="/home/permission" class="btn btn-outline-secondary"> Cancle </a>
    </div>
</form>
@endsection
