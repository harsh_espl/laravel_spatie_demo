<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Role::create(['name'=>'writer']);
        // Permission::create(['name'=>'edit articales']);
        // Permission::create(['name'=>'edit post']);
        // $role = Role::findById(1);
        // $permission = Permission::all();
        // return $permission;
        // $permission = Permission::create(['name'=>'write post']);
        // $role->givePermissionTo($permission);
        // $permission->removeRole($role);
        // $role->revokePermissionTo($permission);
        // $role->syncPermissions($permission);

        // Permission::create(['name'=>'read:admin']);

        // Role::create(['name'=>'writer']);
        // Role::create(['name'=>'editer']);
        // Role::create(['name'=>'publisher']);
        
        // Permission::create(['name'=>'write post']);
        // Permission::create(['name'=>'edit post']);
        // Permission::create(['name'=>'publish post']);

        // $role = Role::where('name','admin')->first();
        // $permission = $role->syncPermissions(Permission::all());        

        return view('home');
    }
}
