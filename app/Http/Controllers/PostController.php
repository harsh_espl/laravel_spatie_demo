<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PostController extends Controller
{
    public function index()
    {        
        $items = DB::table('role_has_permissions')        
        ->join('permissions','role_has_permissions.permission_id','=','permissions.id')
        ->join('roles','role_has_permissions.role_id','=','roles.id')
        ->select('permissions.name as permission','roles.name as role','permissions.id')
        ->get();

        $permission = Permission::all();
        
        return view('permission',compact('items','permission'));
    }

    public function create()
    {
        $role = Role::all();
        $data = new Permission();
        return view('permission.addForm',compact('role','data'));
    }

    public function store()
    {
        $name = request()->validate([
            'permission_name' => 'required',
            'select_role' => 'required_with:permission_name' 
        ]);
        
        $data = Permission::findOrCreate(request('permission_name'));
        
        foreach(request('select_role') as $item)
        {
            $role = Role::findById($item);
            Permission::findById($data->id)->assignRole($role->name);
        } 
        Permission::findById($data->id)->assignRole('admin');
        return redirect('/home/permission');
    }

    public function destroy($id)
    {
        Permission::findById($id)->delete();
        return redirect('home/permission');
    }

    public function edit($id)
    {
        $role = Role::all();
        $data = Permission::findById($id);
        $rhp = [];
        foreach($data->roles as $item)
        {
            array_push($rhp, $item->id);
        }
        
        return view('permission.editForm',compact('role','data','rhp'));
    }

    public function update()
    {
        Permission::findById(request('id'))->update(['name'=>request('permission_name')]);
        DB::table('role_has_permissions')->where('permission_id',request('id'))->delete();
        
        foreach(request('select_role') as $item)
        {
            $role = Role::findById($item);
            Permission::findById(request('id'))->assignRole($role->name);
        } 
        Permission::findById(request('id'))->assignRole('admin');
        return redirect('/home/permission');
    }
}
